# -*- coding: utf-8 -*-

import turtle,random

def fabriqueTortue(position) :
    """Fabrique une tortue à la position donnée (x,y),
    lui donne l'attribut vitesse maximale,
    et la cache."""
    t=turtle.Turtle()
    t.speed(0)
    t.ht()
    t.up()
    t.goto(position)
    t.down()
    return t
    

def rectangle (position_bas_gauche,longueur,largeur,tortue=None,
               couleur=(0,0,0),remplissage=False) :
    """ Trace un rectangle, connaissant la position inférieure gauche, 
    sa longueur et sa largeur. Si une tortue est passée en paramètre, 
    utilise cette tortue, sinon, crée une nouvelle tortue.
    La couleur est donnée sous la forme d'un triplet RGB, chaque valeur étant 
    entre 0 et 255. Le remplissage est désactivé par défaut."""
    if tortue==None :
        traceuse=fabriqueTortue(position_bas_gauche)
    else :
        traceuse=tortue
    
    turtle.colormode(255)
    
    traceuse.color(couleur)
    if remplissage : traceuse.begin_fill()
    for i in range(2) :
        traceuse.forward(longueur)
        traceuse.left(90)
        traceuse.forward(largeur)
        traceuse.left(90)
    if remplissage : traceuse.end_fill()
    del(traceuse) #Suppression de la tortue locale

def rectanglePenche(position_bas_gauche,longueur,largeur,orientation=0,tortue=None,
               couleur=(0,0,0),remplissage=False) :
    """Sur la base de la fonction rectangle, trace un rectangle dont 
    l'orientation est  précisée, selon la norme standard de turtle."""
    if tortue==None :
        traceuse=fabriqueTortue(position_bas_gauche)
    else :
        traceuse=tortue
    traceuse.left(orientation)
    rectangle(position_bas_gauche,longueur,largeur,tortue=traceuse,
              couleur=couleur,remplissage=remplissage)

def exit_and_save() :
    """Fonction appelée au clique dans la fenêtre pour fermer et éventuellement
    sauvegarder l'image obtenue.
    Les détails de cette fonctions ne sont pas obligatoires à comprendre.
    """
    global Encore
    Encore=False
    fileName= ecran.textinput("Alert","Nom du fichier de sauvegarde:")
    if fileName!=None :
        turtle.getscreen().getcanvas().postscript(file=f'{fileName}.ps')


if __name__=="__main__" :
    hauteur_ecran = 480#hauteur de la fenetre
    largeur_ecran = 640#largeur de la fenetre
    ecran = turtle.Screen()#scene est la fenetre dans laquelle évolueront les tortues
    ecran.setup(width=largeur_ecran, height=hauteur_ecran, startx=100, starty=100)#règle les caractéristiques de l'écran
    Encore =True
    while Encore :
        pos=(random.randint(-320,320),random.randint(-240,240))
        longueur=random.randint(50,200)
        largeur=random.randint(20,100)
        orientation=random.randint(-90,90)
        rectanglePenche(pos,longueur,largeur,orientation,
                        couleur=(random.randint(0,255),
                        random.randint(0,50),
                        random.randint(200,255)),
                        remplissage=True)
        # La fonction suivante appelle au clique sur l'écran la fonction
        # exit_and_save()
        ecran.onclick(lambda x,y: exit_and_save() )
    ecran.exitonclick()
    
